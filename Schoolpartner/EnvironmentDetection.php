<?php


namespace Schoolpartner;


use \InvalidArgumentException;

class EnvironmentDetection
{

	const CONSOLE_DEV = 'dev-console';
	const CONSOLE_DEV_LOCAL = 'local-console';
	const CONSOLE_TEST = 'test-console';
	const CONSOLE_PRODUCTION = 'prod-console';

	const DEV = 'dev';
	const DEV_LOCAL = 'local';
	const TEST = 'test';
	const PRODUCTION = 'prod';

	private $inputEnvironments = array(self::DEV, self::DEV_LOCAL, self::TEST, self::PRODUCTION);

	private $hostsToEnvironment = array(
		'localhost' => self::DEV_LOCAL,
		//'\.dev[0-9]*\.loc' => self::DEV, // example.dev1.loc
	);

	private $pathsToEnvironment = array(
		'^/srv/apache/vagrant' => self::DEV_LOCAL,
	);

	private $host;

	private $phpSapi;

	private $projectDirectory;

	private $detectedEnvironment;


	public function __construct($host, $phpSapi, $projectDirectory)
	{
		$this->host = $host;
		$this->phpSapi = $phpSapi;
		$this->projectDirectory = $projectDirectory;
	}


	public function setPathsToEnvironment(array $hostsToEnvironment)
	{
		$this->pathsToEnvironment = array();
		foreach ($hostsToEnvironment as $host => $env) {
			$this->checkEnvironment($env);
			$this->hostsToEnvironment[$host] = $env;
		}
	}


	public function setHostsToEnvironment(array $pathsToEnvironment)
	{
		$this->hostsToEnvironment = array();
		foreach ($pathsToEnvironment as $path => $env) {
			$this->checkEnvironment($env);
			$this->hostsToEnvironment[$path] = $env;
		}
	}


	public function getEnvironment()
	{
		if ($this->detectedEnvironment !== NULL) {
			return $this->detectedEnvironment;
		}

		if ($this->phpSapi === 'cli') {
			$this->detectByPath();
			if ($this->detectedEnvironment === NULL) { // assume localhost
				$this->detectedEnvironment = self::DEV_LOCAL;
			}
			return $this->detectedEnvironment = $this->detectedEnvironment . '-console';
		}

		if ($this->host !== NULL) {
			$this->detectByHost();
		} else {
			$this->detectByPath();
		}

		if ($this->detectedEnvironment === NULL) {
			throw new \RuntimeException('Cannot detect environment.');
		}
		return $this->detectedEnvironment;
	}


	private function detectByHost()
	{
		if ($this->host === NULL) {
			return;
		}

		foreach ($this->hostsToEnvironment as $pattern => $environment) {
			if (preg_match('#' . $pattern . '$#iD', $this->host) === 1) {
				$this->detectedEnvironment = $environment;
				return;
			}
		}
	}


	private function detectByPath()
	{
		if ($this->projectDirectory === NULL) {
			return;
		}

		foreach ($this->pathsToEnvironment as $pattern => $environment) {
			if (preg_match('#' . $pattern . '#i', $this->projectDirectory)) {
				$this->detectedEnvironment = $environment;
				return;
			}
		}
	}


	private function checkEnvironment($env)
	{
		if (!in_array($env, $this->inputEnvironments)) {
			throw new InvalidArgumentException("Unknown enviroment '$env'. Please use one of these: " . implode(", ", $this->inputEnvironments) . ".");
		}
	}

}